package session

import (
	"context"
	"errors"
	"multi-acquire-api-gateway.nextpay.global/utils"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	redisClient *redis.Client
)

type Session struct {
	Id        string
	SecretKey string
}

func NewRedisClient(host, password string) *redis.Client {
	if redisClient == nil {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     host,
			Password: password,
		})
	}

	return redisClient
}

func GetRedisClient() (*redis.Client, error) {
	if redisClient == nil {
		return nil, errors.New("DB not connected yet")
	}

	return redisClient, nil
}

func CreateSession() (s Session, err error) {
	s = Session{
		util.NewUUID(), util.GenerateSecretKey(),
	}

	err = redisClient.Set(context.Background(), "session:"+s.Id, s.SecretKey, 15*60*time.Second).Err()
	if err != nil {
		return
	}

	return s, nil
}

func GetSecretKeyById(id string) (val string, err error) {
	val, err = redisClient.Get(context.Background(), "session:"+id).Result()
	if err != nil {
		return
	}
	return
}

func ExtendExpireTime(id string) (err error) {
	err = redisClient.Expire(context.Background(), "session:"+id, 15*60*time.Second).Err()
	return
}
