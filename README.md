# MACQ - Payment API Gateway
## I. Init Session
**Endpoint**: /init-session  
**Method**: POST  
**Header**: no extra headers  
**Request Body**: 
```JSON
None
```

**Response Body**:
```JSON
{
  "sessionId": "ac4b5e05-00c2-4447-909f-58240b2d71e7",
  "sessionSecretKey": "588t9qjpk8pw4nqyp2c7rm1c7zr5oi4z"
}
```  

## II. Dev APIs(for testing services)
### **Encode API**
**Endpoint**: /test/encode  
**Method**: POST  
**Header**: no extra headers  
**Request Body**: 
```JSON
{
	"value": "{\"name\": \"DaiNghia\", \"age\": 22}",
	"key": "588t9qjpk8pw4nqyp2c7rm1c7zr5oi4z"
}
```

**Response Body**:
```JSON
{
  "encoded": "1974b041b534d9c820491cf1692904c9107c7a219101cce7d21a487d13a71b8b"
}
```  

### **Decode API**
**Endpoint**: /test/decode  
**Method**: POST  
**Header**: no extra headers  
**Request Body**: 
```JSON
{
	"value": "edbb21d909348401cd4c12453afa8ece7e5993e9061aca44e8b58ef83590be8fd23f27e83b4ebee45e4b796710551132",
	"key": "588t9qjpk8pw4nqyp2c7rm1c7zr5oi4z"
}
```

**Response Body**:
```JSON
{
    "decoded": "{\"value\":\"Hello DaiNghia - Your Age: 22\"}"
}
```  

## III. How to call another APIs
**Endpoint**: /v1/api/custom-endpoint  
**Method**: POST  
**Header**:
```Plain
x-session-id: "sessionId" // Required
```  
**Request Body**: all data  
```JSON
{
	"value" :"1974b041b534d9c820491cf1692904c9107c7a219101cce7d21a487d13a71b8b" // Data for calling gRPC function
}
```

**Response Body**:
```JSON
{
  "value": "edbb21d909348401cd4c12453afa8ece7e5993e9061aca44e8b58ef83590be8fd23f27e83b4ebee45e4b796710551132" // Data returned from gRPC
}
```  

## IV. How to run project
### 1. Prerequisite 

* `protoc` in system $PATH
* Have git access to proto files repository 

### 2. Build
```console
# Get proto files from specific TAG
make PROTO_SVC_TAG="1.0.0" checkout
# Build executable
make all
```
If you want to build using Docker:
```
# Get proto files from specific TAG
make PROTO_SVC_TAG="1.0.0" checkout
# Build executable
make build-docker
```

### 3. Run
```console
make run
```