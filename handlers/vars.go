package handler

import "flag"

var (
	grpcServerEndpoint        = flag.String("grpc-server-endpoint", "localhost:50051", "gRPC server endpoint")
	iv                 []byte = []byte("0000000000000000")
	nonWrapAPIs               = []string{
		"/init-session",
		"/decode",
		"/encode",
	}
)
