package handler

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"log"
	"multi-acquire-api-gateway.nextpay.global/session"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/kataras/iris"
	"google.golang.org/grpc"
	"multi-acquire-api-gateway.nextpay.global/gen/api"
	util "multi-acquire-api-gateway.nextpay.global/utils"
)

// InitHTTPWrapper init http wrapper and grpc services
func InitHTTPWrapper(ctx context.Context, app *iris.Application) {
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	if err := api.RegisterUserServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts); err != nil {
		log.Fatalf("failed to register User service: err=%v", err)
	}

	if err := api.RegisterMerchantServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, opts); err != nil {
		log.Fatalf("failed to register Merchant service: err=%v", err)
	}

	app.WrapRouter(func(w http.ResponseWriter, r *http.Request, httpFunc http.HandlerFunc) {
		if util.Contains(nonWrapAPIs, r.URL.Path) {
			httpFunc.ServeHTTP(w, r)
			return
		}

		// Check for session id
		wrapperCtx := app.ContextPool.Acquire(w, r)
		sessionId := r.Header.Get("x-session-id")
		if sessionId == "" {
			wrapperCtx.StatusCode(iris.StatusBadRequest)
			wrapperCtx.WriteString("Access denied: no session id found")
			return
		}

		// Get corresponding secret key from session
		secretKey, err := session.GetSecretKeyById(sessionId)
		if err != nil {
			wrapperCtx.StatusCode(iris.StatusInternalServerError)
			wrapperCtx.WriteString("Your session has been expired")
			return
		}

		// Extend session lifecycle
		session.ExtendExpireTime(sessionId)

		// Decrypt request body
		decryptedBody := util.DecryptRequestBody(r.Body, []byte(secretKey), iv)
		r.Body.Close()
		r.Body = ioutil.NopCloser(decryptedBody)

		// Wrap response write by ResponseWriterWrapper
		responseWriter := &ResponseWriterWrapper{
			ResponseWriter: w,
			Buf:            &bytes.Buffer{},
		}
		mux.ServeHTTP(responseWriter, r)

		// Copy responseWriter.Buf to w(default response writer)
		_, _ = io.Copy(w, util.EncryptResponseBody(responseWriter.Buf, []byte(secretKey), iv))
	})
}
