package handler

import (
	"github.com/kataras/iris"
	"multi-acquire-api-gateway.nextpay.global/crypto"
)

// DevDecode aes-256-gcm decrypt for dev purpose
func DevDecode(ctx iris.Context) {
	var body map[string]string
	err := ctx.ReadJSON(&body)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	decoded, _ := crypto.AesDecrypt([]byte(body["value"]), []byte(body["key"]), iv, crypto.ZeroUnPadding)

	ctx.JSON(iris.Map{"decoded": decoded})
}

// DevEncode aes-256-gcm encrypt for dev purpose
func DevEncode(ctx iris.Context) {
	var body map[string]string
	err := ctx.ReadJSON(&body)

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	encoded, _ := crypto.AesEncrypt([]byte(body["value"]), []byte(body["key"]), iv, crypto.ZeroPadding)

	ctx.JSON(iris.Map{"encoded": encoded})
}
