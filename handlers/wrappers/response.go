package wrapper

import (
	"bytes"
	"net/http"
)

// ResponseWriterWrapper wrap http response to encrypt response body
type ResponseWriterWrapper struct {
	http.ResponseWriter
	Buf *bytes.Buffer
}

func (erw *ResponseWriterWrapper) Write(p []byte) (int, error) {
	return erw.Buf.Write(p)
}
