package handler

import (
	"github.com/kataras/iris"
	"multi-acquire-api-gateway.nextpay.global/session"
)

// InitSession init new user sessions
func InitSession(ctx iris.Context) {
	// Generate new session
	session, err := session.CreateSession()
	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	var body map[string]string

	err = ctx.ReadJSON(&body)
	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	ctx.JSON(iris.Map{
		"sessionId":        session.Id,
		"sessionSecretKey": session.SecretKey,
	})
}

// GetSession get session secret for testing purpose
func GetSession(ctx iris.Context) {
	secret, err := session.GetSecretKeyById(ctx.Params().Get("id"))
	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	ctx.JSON(iris.Map{
		"secretKey": secret,
	})
}
