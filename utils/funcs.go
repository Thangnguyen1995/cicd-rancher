package util

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"math/rand"

	"github.com/google/uuid"
	"multi-acquire-api-gateway.nextpay.global/crypto"
)

// DecryptRequestBody decrypt request body which encrypted from client
func DecryptRequestBody(body io.ReadCloser, key, iv []byte) *bytes.Buffer {
	// Decode body to json
	var bodyJSON map[string]string
	err := json.NewDecoder(body).Decode(&bodyJSON)
	if err != nil {
		log.Fatal("Decode bi loi", err)
	}

	decryptedData, _ := crypto.AesDecrypt([]byte(bodyJSON["value"]), key, iv, crypto.ZeroUnPadding)
	return bytes.NewBuffer([]byte(decryptedData))
}

// EncryptResponseBody encrypt response body for sending to client
func EncryptResponseBody(buf *bytes.Buffer, key, iv []byte) *bytes.Buffer {
	encryptedResponseData, _ := crypto.AesEncrypt(buf.Bytes(), key, iv, crypto.ZeroPadding)
	responseBody := []byte(`{ "value" : "` + encryptedResponseData + `"}`)
	return bytes.NewBuffer(responseBody)
}

// NewUUID generate new uuid
func NewUUID() string {
	return uuid.New().String()
}

// NewSessionSecret generate new session secret
func NewSessionSecret() string {
	seed := base64Encode([]byte(uuid.New().String()))
	return string(seed)
}

//Contains Check if string includes in slice
func Contains(s []string, item string) bool {
	for _, sItem := range s {
		if sItem == item {
			return true
		}
	}
	return false
}

func base64Encode(message []byte) []byte {
	encoded := make([]byte, base64.StdEncoding.EncodedLen(len(message)))
	base64.StdEncoding.Encode(encoded, message)
	return encoded
}

func base64Decode(message []byte) []byte {
	decoded := make([]byte, base64.StdEncoding.DecodedLen(len(message)))
	base64.StdEncoding.Decode(decoded, message)
	return decoded
}

func GenerateSecretKey() string {
	seed := "1234567890qwertyuiopasdfghjklzxcvbnm"
	key := ""

	for i := 0; i < 32; i++ {
		key += string(seed[rand.Intn(len(seed))])
	}

	return key
}
