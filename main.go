package main

import (
	"context"
	"flag"
	"multi-acquire-api-gateway.nextpay.global/session"
	"os"

	_ "net/http/pprof"

	"github.com/golang/glog"
	"github.com/joho/godotenv"
	"github.com/kataras/iris"

	handler "multi-acquire-api-gateway.nextpay.global/handlers"
)

func registerServiceEndpoints() {

}

func run() error {
	// Load .env file if exists
	_ = godotenv.Load()

	// Connect to session server
	session.NewRedisClient(os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PASS"))

	// Init api server
	app := iris.New()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	handler.InitHTTPWrapper(ctx, app)

	// Init session api
	app.Post("/v1/api/init-session", handler.InitSession)
	app.Get("/test/get-session/{id:string}", handler.GetSession)

	// APIs for testing purposes
	app.Post("/test/decode", handler.DevDecode)
	app.Post("/test/encode", handler.DevEncode)

	return app.Run(iris.Addr(":9999"))
}

func main() {
	flag.Parse()
	defer glog.Flush()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}
