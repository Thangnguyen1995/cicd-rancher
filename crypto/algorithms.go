package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
)

func AesEncrypt(origData, key []byte, iv []byte, paddingFunc func([]byte, int) []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	blockSize := block.BlockSize()
	origData = paddingFunc(origData, blockSize)

	blockMode := cipher.NewCBCEncrypter(block, iv)
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return hex.EncodeToString(crypted), nil
}

func AesDecrypt(crypted, key, iv []byte, unPaddingFunc func([]byte) []byte) (string, error) {
	cryptedString, _ := hex.DecodeString(string(crypted))
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	blockMode := cipher.NewCBCDecrypter(block, iv)
	origData := make([]byte, len(cryptedString))
	blockMode.CryptBlocks(origData, cryptedString)
	origData = unPaddingFunc(origData)
	return string(origData), nil
}
