export GOBIN ?= $(shell pwd)/bin
export PATH := $(PATH):$(GOBIN)

APP_NAME := ma-api-gateway
# Generated source files folder
GEN_SRC_DIR := gen/api
PROTO_SVC_ROOT := api/protobuf
PROTO_SVC_DIR := services
PROTO_SVC_SUBDIR := merchant user
# Branch to checkout service proto files
PROTO_SVC_REPO := http://gitlab.saobang.vn/multi-acquire/multi-acquirer-proto-files.git
PROTO_SVC_TAG = 1.1.15

# can be override by using: make -e DOCKER_REGISTRY=foo
DOCKER_REGISTRY = registry.saobang.vn
DOCKER_TAG = latest

.PHONY: all
all: clean build

.PHONY: clean
clean:
	$(info --> Clean working directory)
	@rm -rf gen/ bin/

.PHONY: checkout
checkout:
	$(info --> Checkout/Update proto service dir from branch $(PROTO_SVC_TAG))
	@[ -d $(PROTO_SVC_ROOT)/$(PROTO_SVC_DIR) ] || (cd $(PROTO_SVC_ROOT) && git clone --single-branch --branch $(PROTO_SVC_TAG) $(PROTO_SVC_REPO) $(PROTO_SVC_DIR)) &&\
	cd $(PROTO_SVC_ROOT)/$(PROTO_SVC_DIR) && git pull

.PHONY: tools
tools:
	$(info --> Install required tools)
	@go install github.com/golang/protobuf/protoc-gen-go
	@go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	@go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway

$(GEN_SRC_DIR):
	$(info --> Create folder for generated source file)
	@mkdir -p $@

.PHONY: proto-service
proto-service: tools | $(GEN_SRC_DIR)
	$(info --> Generate service source from proto files)
	@$(foreach dir,$(PROTO_SVC_SUBDIR), ( \
		protoc --proto_path=$(PROTO_SVC_ROOT) --go_out=plugins=grpc:$(GEN_SRC_DIR) $(PROTO_SVC_ROOT)/$(PROTO_SVC_DIR)/$(dir)/*.proto \
	) &&) true

.PHONY: proto-gateway
proto-gateway: proto-service
	$(info --> Generate gateway source from proto files)
	@$(foreach dir,$(PROTO_SVC_SUBDIR), ( \
		protoc --proto_path=$(PROTO_SVC_ROOT) --grpc-gateway_out=logtostderr=true,paths=source_relative:$(GEN_SRC_DIR) $(PROTO_SVC_ROOT)/$(PROTO_SVC_DIR)/$(dir)/*.proto &&\
		mv $(GEN_SRC_DIR)/$(PROTO_SVC_DIR)/$(dir)/*.go $(GEN_SRC_DIR) && rm -rf $(GEN_SRC_DIR)/$(PROTO_SVC_DIR)/$(dir) \
	) &&) true
	@rm -rf $(GEN_SRC_DIR)/$(PROTO_SVC_DIR)

.PHONY: build
build: proto-gateway
	$(info --> Build executable binary file...)
	@go build -ldflags="-s -w" -o bin/$(APP_NAME)

.PHONY: run
run:
	@./bin/$(APP_NAME)

docs: checkout
	@mkdir -p $@
	@$(foreach dir,$(PROTO_SVC_SUBDIR), ( \
		protoc --proto_path=$(PROTO_SVC_ROOT) --swagger_out=logtostderr=true:./$@ $(PROTO_SVC_ROOT)/$(PROTO_SVC_DIR)/$(dir)/*.proto \
	) &&) true

.PHONY: build-docker
build-docker:
	$(info --> Build docker image)
	@docker build --build-arg "APP_NAME=$(APP_NAME)" -t $(DOCKER_REGISTRY)/$(APP_NAME):$(DOCKER_TAG) -f Dockerfile .