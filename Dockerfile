FROM golang:1.14.4-buster as builder

ENV GOOS=linux \
    GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0

RUN apt-get -y update && \
    apt-get install -y protobuf-compiler && \
    apt-get clean

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN make all


FROM scratch
ARG APP_NAME=ma-api-gateway
COPY --from=builder /app/bin/${APP_NAME} app
# Command to run the executable
ENTRYPOINT ["./app"]